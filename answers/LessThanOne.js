import { Text, SafeAreaView } from "react-native";
import {styles as baseStyles, styles} from "../styles";

const LessThanOne = () => (
  <SafeAreaView style={styles.container}>
    <Text style={baseStyles.innerContainer}>CT / MRI in 6 months, then every 2 years if no change</Text>
  </SafeAreaView>
);
export default LessThanOne;
