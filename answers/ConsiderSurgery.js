import { Text, SafeAreaView } from "react-native";
import {styles as baseStyles, styles} from "../styles";

const ConsiderSurgery = () => (
  <SafeAreaView style={styles.container}>
    <Text style={baseStyles.innerContainer}>Consider surgery, if clinically appropriate</Text>
  </SafeAreaView>
);
export default ConsiderSurgery;
