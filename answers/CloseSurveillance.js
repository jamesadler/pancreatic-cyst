import { Text, SafeAreaView } from "react-native";
import {styles as baseStyles, styles} from "../styles";

const CloseSurveillance = () => (
  <SafeAreaView style={styles.container}>
    <Text style={baseStyles.innerContainer}>
      Close surveillance alternating MRI with EUS every 3-6 months. Strongly
      consider surgery in young, fit patients
    </Text>
  </SafeAreaView>
);
export default CloseSurveillance;
