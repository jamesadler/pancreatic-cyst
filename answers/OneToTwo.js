import { Text, SafeAreaView } from "react-native";
import {styles as baseStyles, styles} from "../styles";

const OneToTwo = () => (
  <SafeAreaView style={styles.container}>
    <Text style={baseStyles.innerContainer}>
      CT / MRI 6 months x 1 year yearly x 2 years, then lengthen interval up to
      2 years if no change
    </Text>
  </SafeAreaView>
);
export default OneToTwo;
