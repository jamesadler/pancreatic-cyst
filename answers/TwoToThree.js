import { Text, SafeAreaView } from "react-native";
import {styles as baseStyles, styles} from "../styles.js";

const TwoToThree = () => (
  <SafeAreaView style={styles.container}>
    <Text style={baseStyles.innerContainer}>
      EUS in 3-6 months, then lengthen interval up to 1 year, alternating MRI
      with EUS as appropriate. Consider surgery in young, fit patients with need
      for prolonged surveillance
    </Text>
  </SafeAreaView>
);
export default TwoToThree;
