import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import QuestionHighRiskStigmata from "./questions/QuestionHighRiskStigmata";
import QuestionWorrisomeFeatures from "./questions/QuestionWorrisomeFeatures";
import ConsiderSurgery from "./answers/ConsiderSurgery";
import QuestionFeaturesPresent from "./questions/QuestionFeaturesPresent";
import CloseSurveillance from "./answers/CloseSurveillance";
import QuestionSizeOfCyst from "./questions/QuestionSizeOfCyst";
import LessThanOne from "./answers/LessThanOne";
import OneToTwo from "./answers/OneToTwo";
import TwoToThree from "./answers/TwoToThree";

const Stack = createNativeStackNavigator();

const App = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name="Home" component={QuestionHighRiskStigmata} />
      <Stack.Screen
        name="YesHighRiskStigmata"
        component={ConsiderSurgery}
        options={{ title: "Yes, High Risk Stigmata Present" }}
      />
      <Stack.Screen
        name="NoHighRiskStigmata"
        component={QuestionWorrisomeFeatures}
        options={{ title: "No High Risk Stigmata Present" }}
      />
      <Stack.Screen
        name="YesWorrisomeFeatures"
        component={QuestionFeaturesPresent}
        options={{ title: "Yes, Worrisome Features Present" }}
      />
      <Stack.Screen
        name="NoWorrisomeFeatures"
        component={QuestionSizeOfCyst}
        options={{ title: "No Worrisome Features Present" }}
      />
      <Stack.Screen
        name="Inconclusive"
        component={CloseSurveillance}
        options={{ title: "Inconclusive Features" }}
      />
      <Stack.Screen
        name="LessThanOne"
        component={LessThanOne}
        options={{ title: "Largest Cyst is < 1 cm" }}
      />
      <Stack.Screen
        name="OneToTwo"
        component={OneToTwo}
        options={{ title: "Largest Cyst is 1-2 cm" }}
      />
      <Stack.Screen
        name="TwoToThree"
        component={TwoToThree}
        options={{ title: "Largest Cyst is 2-3 cm" }}
      />
      <Stack.Screen
        name="GreaterThanThree"
        component={CloseSurveillance}
        options={{ title: "Largest Cyst is > 3 cm" }}
      />
    </Stack.Navigator>
  </NavigationContainer>
);
export default App;
