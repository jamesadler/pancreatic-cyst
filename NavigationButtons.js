import { StyleSheet, SafeAreaView, Button } from "react-native";

const NavigationButtons = ({
  navigation,
  yesRoute,
  yesLabel = "Yes",
  noRoute,
  noLabel = "No",
}) => (
  <SafeAreaView style={styles.actionContainer}>
    <Button
      onPress={() => navigation.navigate(yesRoute)}
      title={yesLabel}
      color="#841584"
      accessibilityLabel={yesLabel}
    />
    <Button
      onPress={() => navigation.navigate(noRoute)}
      title={noLabel}
      color="#841584"
      accessibilityLabel={noLabel}
    />
  </SafeAreaView>
);
export default NavigationButtons;

const styles = StyleSheet.create({
  actionContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 16,
    width: "100%",
  },
});
