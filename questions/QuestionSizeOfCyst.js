import { Button, Text, SafeAreaView, View } from "react-native";
import { styles } from "../styles.js";

const QuestionSizeOfCyst = ({ navigation }) => (
  <SafeAreaView style={styles.container}>
    <Text>What is the size of largest cyst?</Text>
    <View style={styles.actionContainer}>
      <Button
        onPress={() => navigation.navigate("LessThanOne")}
        title={"<1 cm"}
        color="#841584"
        accessibilityLabel={"<1 cm"}
      />
      <Button
        onPress={() => navigation.navigate("OneToTwo")}
        title={"1-2 cm"}
        color="#841584"
        accessibilityLabel={"1-2 cm"}
      />
      <Button
        onPress={() => navigation.navigate("TwoToThree")}
        title={"2-3 cm"}
        color="#841584"
        accessibilityLabel={"2-3 cm"}
      />
      <Button
        onPress={() => navigation.navigate("GreaterThanThree")}
        title={">3 cm"}
        color="#841584"
        accessibilityLabel={">3 cm"}
      />
    </View>
  </SafeAreaView>
);

export default QuestionSizeOfCyst;
