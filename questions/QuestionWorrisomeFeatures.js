import { StyleSheet, Text, SafeAreaView, View } from "react-native";
import NavigationButtons from "../NavigationButtons";
import { styles as baseStyles } from "../styles.js";

const QuestionWorrisomeFeatures = ({ navigation }) => (
  <SafeAreaView style={baseStyles.container}>
    <Text style={styles.headerText}>
      Are any of the following{" "}
      <Text style={styles.headerTextEmphasize}>"worrisome features"</Text>{" "}
      present?
    </Text>
    <Text style={baseStyles.innerContainer}>
      <Text style={styles.detailContainer}>
        <Text style={styles.detailHeader}>Clinical:</Text> Pancreatitis{" "}
        <View style={baseStyles.superScriptView}>
          <Text style={baseStyles.superScriptText}>a</Text>
        </View>
      </Text>
      {"\n"}
      <Text style={styles.detailContainer}>
        <Text style={styles.detailHeader}>Imagining: </Text>i) cyst &#8805; 3 cm
        {"\n"}
        <Text style={styles.detailText}>
          ii) enhancing mural nodule &lt; 5 mm,{"\n"}
        </Text>
        <Text style={styles.detailText}>
          iii) thickened/enhancing cyst walls,{"\n"}
        </Text>
        <Text style={styles.detailText}>iv) main duct size 5-9 mm,{"\n"}</Text>
        <Text style={styles.detailText}>
          v) abrupt change in caliber of pancreatic duct with distal pancreatic
          atrophy,{"\n"}
        </Text>
        <Text style={styles.detailText}>vi) lymphadenopathy,{"\n"}</Text>
        <Text style={styles.detailText}>
          vii) increased serum level of CA 19-9,{"\n"}
        </Text>
        <Text style={styles.detailText}>
          viii) cyst growth rate &#8805; 5 mm / 2 years
        </Text>
      </Text>
    </Text>
    <NavigationButtons
      navigation={navigation}
      yesRoute="YesWorrisomeFeatures"
      yesLabel="If yes, perform endoscopic ultrasound"
      noRoute="NoWorrisomeFeatures"
    />
    <View style={baseStyles.innerContainer}>
      <Text style={baseStyles.superScriptText}>a</Text><Text style={baseStyles.superScriptDescription}>Pancreatitis may be an indication for surgery for relief of symptoms</Text>
    </View>
  </SafeAreaView>
);
export default QuestionWorrisomeFeatures;

const styles = StyleSheet.create({
  headerText: {
    fontWeight: "bold",
    textDecorationLine: "underline",
  },
  headerTextEmphasize: {
    fontStyle: "italic",
  },
  detailContainer: {
    width: "100%",
  },
  detailHeader: {
    fontWeight: "bold",
    fontStyle: "italic",
  },
  detailText: {
    marginLeft: 78,
    // display: "block",
  },

});
