import { StyleSheet, Text, SafeAreaView, View } from "react-native";
import NavigationButtons from "../NavigationButtons";
import { styles as baseStyles } from "../styles.js";

const QuestionFeaturesPresent = ({ navigation }) => (
  <SafeAreaView style={baseStyles.container}>
    <Text style={styles.headerText}>Are any of these features present?</Text>
    <Text style={baseStyles.innerContainer}>
      <Text style={styles.detailContainer}>
        i) Definite mural nodule(s) &#8805; 5 cm{" "}
        <View style={baseStyles.superScriptView}>
          <Text style={baseStyles.superScriptText}>b</Text>
        </View>
        {"\n"}
        ii) Main duct features suspicious for involvement{" "}
        <View style={baseStyles.superScriptView}>
          <Text style={baseStyles.superScriptText}>c</Text>
        </View>
        {"\n"}
        iii) Cytology: suspicious or positive for malignancy
      </Text>
    </Text>
    <NavigationButtons
      navigation={navigation}
      yesRoute="Inconclusive"
      yesLabel="Inconclusive"
      noRoute="NoWorrisomeFeatures"
    />
    <View style={baseStyles.innerContainer}>
      <Text style={baseStyles.superScriptText}>b</Text><Text style={baseStyles.superScriptDescription}>Differential diagnosis includes mucin. Mucin can move with change in patient position, may be dislodged on cyst lavage and does not have Doppler flow. Features of true tumor nodule include lack of mobility, presence of Doppler flow and fine need aspiration (FNA) of nodule showing tumor tissue.</Text>
    </View>
    <View style={baseStyles.innerContainer}>
      <Text style={baseStyles.superScriptText}>c</Text><Text style={baseStyles.superScriptDescription}>Presence of any one of thickened walls, intraductal mucin or mural nodules is suggestive of main duct involvement. In their absence main duct involvement is inconclusive. </Text>
    </View>
  </SafeAreaView>
);
export default QuestionFeaturesPresent;

const styles = StyleSheet.create({
  headerText: {
    fontWeight: "bold",
  },
  headerTextEmphasize: {
    fontStyle: "italic",
  },
  detailContainer: {
    width: "100%",
  },
  detailHeader: {
    fontWeight: "bold",
    fontStyle: "italic",
  },
  detailText: {
    marginLeft: 78,
    // display: "block",
  },
});
