import { StyleSheet, Text, SafeAreaView } from "react-native";
import NavigationButtons from "../NavigationButtons";
import { styles as baseStyles } from "../styles.js";

const QuestionHighRiskStigmata = ({ navigation }) => (
  <SafeAreaView style={baseStyles.container}>
    <Text style={styles.headerText}>
      Are any of the following{" "}
      <Text style={styles.headerTextEmphasize}>"high-risk stigmata"</Text> of
      malignancy present?
    </Text>
    <Text style={baseStyles.innerContainer}>
      i) obstructive jaundice in a patient with cystic lesion of the head of the
      pancreas,{"\n"}
      ii) enhancing mural nodule &#8805; 5 mm,{"\n"}
      iii) main pancreatic duct &#8805; 10 mm{"\n"}
    </Text>

    <NavigationButtons
      navigation={navigation}
      yesRoute="YesHighRiskStigmata"
      noRoute="NoHighRiskStigmata"
    />
  </SafeAreaView>
);
export default QuestionHighRiskStigmata;

const styles = StyleSheet.create({
  headerText: {
    fontWeight: "bold",
    textAlign: "center",
    textDecorationLine: "underline",
  },
  headerTextEmphasize: {
    fontStyle: "italic",
  },
});
