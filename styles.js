import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#fff",
    flex: 1,
    justifyContent: "center",
  },
  innerContainer: {
    paddingLeft: 16,
    paddingRight: 16
  },
  actionContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 16,
    width: "100%",
  },
  superScriptView: {
    flexDirection: "row",
    alignItems: "flex-start",
  },
  superScriptText: {
    fontWeight: "bold",
    fontSize: 11,
    lineHeight: 18,
  },
  superScriptDescription: {
    fontStyle: "italic",
  }
});

export { styles };
